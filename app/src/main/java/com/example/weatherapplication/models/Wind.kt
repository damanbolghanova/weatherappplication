package com.example.weatherapplication.models

import java.io.Serializable

data class Wind(
        val speed: Double,
        val ged: Int
) : Serializable
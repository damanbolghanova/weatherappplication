package com.example.weatherapplication.models

import java.io.Serializable

data class Sys (
        val type: Int,
        val message: Double,
        val sunrise: Int,
        val sunset: Int
) : Serializable
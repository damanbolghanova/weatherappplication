package com.example.weatherapplication.activities

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.weatherapplication.R
import com.example.weatherapplication.utils.Constants
import com.google.android.gms.location.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class MainActivity : AppCompatActivity() {

    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (!isLocationEnabled()){
            Toast.makeText(this, "Your location provider is turned off. Please turn it on", Toast.LENGTH_SHORT).show()

            val intent = Intent(Settings.ACTION_LOCALE_SETTINGS)
            startActivity(intent)
        }else{
            Toast.makeText(this, "Your location provider is already on", Toast.LENGTH_SHORT).show()
        }

        Dexter.withActivity(this).withPermissions(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
        ).withListener(object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                if (report!!.areAllPermissionsGranted()){
                    requestLocationData()
                }
                if (report.isAnyPermissionPermanentlyDenied){
                    Toast.makeText(this@MainActivity, "U have denied location permission.Please allow it is mandatory.", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                showRationalDialogPermissions()
            }
        }).onSameThread().check()
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    private fun showRationalDialogPermissions(){
        AlertDialog.Builder(this).setMessage("It looks like u have turned off permissions required for this feature. It can be enabled under Application Settings")
                .setPositiveButton("Go To Settings"){
                    _, _ ->
                    try {
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri = Uri.fromParts("package", packageName, null)
                        intent.data = uri

                        startActivity(intent)
                    }catch (e: ActivityNotFoundException){
                        e.printStackTrace()
                    }
                }
                .setNegativeButton("Cancel"){ dialog, _ -> dialog.dismiss() }.show()
    }
    private val mLocationCallback = object : LocationCallback(){
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation : Location = locationResult.lastLocation
            val latitude = mLastLocation.latitude
            Log.i("Current Latitude", "$latitude")
            val longitude = mLastLocation.longitude
            Log.i("Current longitude", "$longitude")

            getLocationWeatherDetails()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationData(){
        val mLocatioRequest = LocationRequest()
        mLocatioRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mFusedLocationClient.requestLocationUpdates(
                mLocatioRequest, mLocationCallback, Looper.myLooper()
        )
    }

    private fun getLocationWeatherDetails(){
        if (Constants.isNetworkAvailable(this@MainActivity)){
            Toast.makeText(this@MainActivity, "U have connected to the Internet. Now u can make an API call.", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this@MainActivity, "No Internet connection available", Toast.LENGTH_SHORT).show()
        }
    }
}
